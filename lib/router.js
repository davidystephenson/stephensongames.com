Router.configure({ 
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  trackPageView: true,
});

Router.route('/', {
  data() { 
    return { 
      designers: Designers.find(),
      games: Games.find(),
      featured: Games.findOne({ featured: true }),
    };
  },
  name: 'home',
  waitOn() { 
    return [
      Meteor.subscribe('designers'),
      Meteor.subscribe('games'),
      Meteor.subscribe('featured')
    ] 
  }
});

Router.route('/designers', {
  data() { return { designers: Designers.find() } },
  name: 'designers',
  waitOn() { return Meteor.subscribe('designers') },
});

Router.route('/designer/:slug', {
  data() { return { designer: Designers.findOne({ slug: this.params.slug }) } },
  name: 'designer',
  waitOn() { return Meteor.subscribe('designer', this.params.slug) },
});

Router.route('/games', {
  data() { return { games: Games.find() } },
  name: 'games',
  waitOn() { return Meteor.subscribe('games') },
});

Router.route('/game/:slug', {
  data() { return { game: Games.findOne({ slug: this.params.slug }) } },
  name: 'game',
  waitOn() { return Meteor.subscribe('game', this.params.slug) },
});
