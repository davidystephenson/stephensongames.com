Designers = new Mongo.Collection('designers');

Designer = Astronomy.createClass({
  collection: Designers,

  name: 'Designer',

  fields: {
    name: 'string',
    email: 'string',
    phone: 'string',
    twitter: 'string',
    bio: 'string',
    site: 'string',
    slug: 'string',
    photo: 'string',
  }
});

if (Designers.find().count() === 0) {
  var david = new Designer();

  david.set('name', 'David Y. Stephenson');
  david.set('email', 'david@davidystephenson.com');
  david.set('phone', '910.874.8740');
  david.set('twitter', 'davidysteph');
  david.set(
    'bio',
    'David Y. Stephenson has been designing games recreationally since childhood. David studied international politics, law, and security at Campbell University. David is currently working as web developer in Arlington, Virginia.'
  );
  david.set('site', 'http://davidystephenson.com');
  david.set('slug', 'david');
  david.set('photo', 'david.png');

  david.save();

  var daniel = new Designer();

  daniel.set('name', 'Daniel G. Stephenson');
  daniel.set('email', 'dstephenson@tamu.edu');
  daniel.set('phone', '919.623.3599');
  daniel.set('bio', 'Daniel G. Stephenson is a doctoral student in economics at Texas A&M University. His research interests include game theory, experimental economics, mechanism design, and behavioral economics. In his (admittedly meager) free time he enjoys reading stories and playing games.');
  daniel.set('site', 'http://danielgstephenson.com');
  daniel.set('slug', 'daniel');
  daniel.set('photo', 'daniel.jpg');

  daniel.save();
}
