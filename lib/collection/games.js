/* global Astronomy, Game, Games, Mongo */

Games = new Mongo.Collection('games')

Game = Astronomy.createClass({
  collection: Games,

  name: 'Game',

  fields: {
    age: 'string',
    amazon: 'string',
    boardGameGeek: 'string',
    components: 'array',
    description: 'string',
    features: 'array',
    name: 'string',
    photo: 'string',
    players: 'string',
    premise: 'string',
    published: 'string',
    publisher: 'string',
    release: 'date',
    repository: 'string',
    slug: 'string',
    summary: 'string',
    time: 'string',
    video: 'string'
  }
})

if (Games.find().count() === 0) {
  var empire = new Game()

  empire.set('name', 'EMPIRE')
  empire.set('time', '30 to 60')
  empire.set('release', new Date('2017-10-26'))
  empire.set(
    'description',
    'A dynamic auction and trading game set in the Age of Imperialism.'
  )
  empire.set(
    'premise',
    'In an age of exploration, rival nations battle for dominance. Discover new lands. Amass great wealth. Gather support. Above all, ensure imperial stability in a time of chaos.'
  )
  empire.set(
    'summary',
    "Empire is a dynamic auction game depicting the struggle between colonial empires. Read the shifting array of available resources to determine which battles are worth fighting. Anticipate your opponents' strategies to avoid their attacks and formulate counters. Success requires finding the most profitable trades and coming out ahead in tense negotiations. Diverse starting set-ups and player powers keep each game unique without allowing luck to determine the winner."
  )
  empire.set('slug', 'empire')
  empire.set(
    'features',
    [
      {
        icon: 'block',
        title: 'No luck, no turns',
        text: "When you play Empire, you don't have to wait for your turn. When you win, you know its because you played well. Empire's selection cards allow players to make decisions simultaneously and intuitively, eliminating down time and removing luck from the game. When we say zero luck, we mean it."
      },
      {
        icon: 'infinity',
        title: 'Infinite replayability',
        text: "Every game of Empire is different. A simple variable setup process gives players a unique and challenging experience every game. Empire's dynamic mechanics interact fluidly with player actions, giving you a reactive global economy that responds to your individual game."
      },
      {
        icon: 'chat',
        title: 'Constant player interaction',
        text: 'In Empire, players are never siloed into their own solitaire experience. Each round throws players against each other in battle, commerce, and politics. Trade any time you want. Negotiate as much or as little as your strategy asks for. How you play the game, and how much of it you spend talking, is totally up to you.'
      },
      {
        icon: 'globe',
        title: 'Variable player powers',
        text: "Control a real historical Empire with their own unique abilities and resources. Each nation is balanced against each other by players' choices. An overpowered character can never gain an unfair advantage."
      },
      {
        icon: 'lab-flask',
        title: 'Innovative play is rewarded',
        text: "Empire's mechanics encourage players to find new and unique approaches to problems. Players are always incentivized to find strategies other players are undervaluing."
      },
      {
        icon: 'emoji-happy',
        title: 'Collaborate <i>and</i> Compete',
        text: 'Empire combines the excitement of a cut-throat race to the top with the satisfaction of working with your friends. Every player fights for themself, but victory requires finding ways to work together.'
      },
      {
        icon: 'open-book',
        title: 'Simple rules, deep strategy',
        text: 'Empire takes 30 minutes for your first game and can be explained in 5 clean, neat, 12 pt font pages. You do not need a complex ruleset or long game time to enjoy high level tactics or diverse gameplay experiences.'
      },
      {
        icon: 'flow-tree',
        title: 'Theme and weight flexible',
        text: "Empire's mechanics can be translated to a variety of settings and time periods, and fit great into currently existing IPs. Empire's modular structure is highly expandable, and fully-designed and tested variants are available for the gamer or publisher who wants more."
      }
    ]
  )
  empire.set(
    'components',
    [
      '100 Resource Chits',
      '100 Support Cards',
      '50 Population Figures',
      '50 Gold Coins',
      '60 Selection Cards',
      '10 Nation Cards',
      '10 Conflict Tiles',
      '1 Diplomacy Token'
    ]
  )
  empire.set('photo', 'empire.jpg')
  empire.set('repository', 'https://bitbucket.org/davidystephenson/empire')
  empire.set('publisher', 'WizKids')
  empire.set('published', 'https://wizkids.com/empires/')
  empire.set('amazon', 'https://www.amazon.com/WizKids-Empires-Game-Board-Games/dp/B073R346SF')
  empire.set('boardGameGeek', 'https://boardgamegeek.com/boardgame/230590/empires')

  empire.save()

  // var fealty = new Game()

  // fealty.set('name', 'FEALTY')
  // fealty.set('time', '15 to 30')
  // fealty.set('players', '4 to 8')
  // fealty.set(
  //   'description',
  //   'A hidden-role social deduction microgame where take the role of noble houses in a medieval kingdom.'
  // )
  // fealty.set(
  //   'premise',
  //   'Noble houses compete to amass power in successive wars for the crown. Seize the throne. Secretly plot to overthrow the king. Investigate your rivals and accuse them before the entire court. Ultimately preserve your honor and ensure the power of your noble house for generations to come.'
  // )
  // fealty.set(
  //   'summary',
  //   'Fealty is a game of alliances, intrigue, and betrayal. Each round, one player reigns as king and other players are nobles. Some nobles are loyalists who work to support the king. Other nobles are traitors who plot to overthrow the king. One traitor is a usurper who aims to seize the throne. The nobles who sacrifice their own power to hope to earn the trust of the king, for good or for evil. At the end of each round, the traitors rise in rebellion against the king, plunging the nation into war. '
  // )
  // fealty.set('slug', 'fealty')
  // fealty.set(
  //   'features',
  //   [
  //     {
  //       icon: 'feather',
  //       title: 'Fast play, light footprint',
  //       text: 'Fealty only takes 15 minutes to play and requires only 17 cards, but provides deep tension and constant social interaction with many opportunities for betrayal, conspiracy, and revenge.'
  //     },
  //     {
  //       icon: 'eye-with-line',
  //       title: 'A social experience with strategic depth',
  //       text: 'Players experience several different roles including the king, the loyalist, and the traitor, each providing a unique strategic experience. Unlike other hidden role games, fealty provides remarkable strategic depth by giving every player an active role in signaling their secret identity, hiding their true character from others, or deducing the hidden nature of their rivals.'
  //     }
  //   ]
  // )
  // fealty.set('video', 'https://www.youtube.com/embed/QNIsjfVhlJM')
  // fealty.set(
  //   'components',
  //   [
  //     '8 House Cards',
  //     '8 Role Cards',
  //     '1 Chancellor Card'
  //   ]
  // )
  // fealty.set('photo', 'fealty.png')
  // fealty.set('repository', 'https://bitbucket.org/davidystephenson/fealty')

  // fealty.save()
}
