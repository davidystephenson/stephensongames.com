Meteor.publish('designer', slug => Designers.find({ slug: slug }));
Meteor.publish('designers', () => Designers.find());

Meteor.publish('game', slug => Games.find({ slug: slug }));
Meteor.publish('games', () => Games.find());
Meteor.publish('featured', () => Games.find({ featured: true }));
