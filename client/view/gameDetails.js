const isEven = (count) => {
  return count % 2 === 0;
};

Template.gameDetails.helpers({
  buildRows(elements) {
    const rows = [];
    elements.map((element, index) => {
      if (isEven(index)) {
        rows.push([element]);
      } else {
        rows.slice(-1)[0].push(element);
      }
    });

    return rows;
  },
});
